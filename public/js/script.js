const xhr = (obj, method, id) => {
  console.log("____XHR___", obj, method, id);
  const xhr = new XMLHttpRequest();
  const url = '/';

  if (method === 'POST') {
    xhr.open("POST", url, true);
  }

  if (method === 'DELETE') {
    xhr.open("POST", '/delete', true);
  }

  xhr.setRequestHeader("Content-type", "application/json");

  xhr.onreadystatechange = function () {//Call a function when the state changes.
    if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
      // Request finished.
      console.log('response  ', xhr.responseText);
      location.reload();
    }
  }

  xhr.send(obj);
}

const convertToReactSyntax = (str) => {
  console.log('String has been converted.');
  //return str.replace(/\s(class=)/g, " className=").replace(/"/g, '\\"');
  return str.replace(/"/g, '\\"');

}

const getData = () => {
  let width = document.getElementById("width").value;
  let height = document.getElementById("height").value;
  let url = document.getElementById("url").value;
  let html = document.getElementById("html").value;
  let scripts = document.getElementById("scripts").value;


  html = convertToReactSyntax(html);
  scripts = convertToReactSyntax(scripts);

  let obj = {
    banners : []
  };

  obj.banners.push({width, height, url, html, scripts});

  xhr(JSON.stringify(obj), 'POST');
}

document.addEventListener('DOMContentLoaded', () => {
  let submitButton = document.getElementById('submit');

  submitButton.addEventListener('click', (e) => {
    e.preventDefault();
    getData();
  });


});