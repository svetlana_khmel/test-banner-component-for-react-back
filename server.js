const express = require('express');
const bodyParser = require('body-parser');
const router = require('./api/routes/index')
const deleteRoute = require('./api/routes/delete')
const api = require('./api/routes/api/index')

const path = require('path');
const cors = require('cors');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/', router);
app.use('/delete', deleteRoute);
app.use('/api', api);

const port = process.env.PORT || 3800;

app.listen(port, function() {
  console.log('Example app listening on port 3800!');
});



