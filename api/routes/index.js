const express = require('express');
const router = express.Router();
const fs = require('fs');
const cors = require('cors');
const path = require('path');
router.use(cors());

const filePath = path.join(__dirname + '../../../data/data.json');

router.get('/', (req, res, next) => {
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (err) return next(err);
    res.render('index', {data: JSON.parse(data)});
  });
});

router.post('/', (req, res, next) => {
   fs.writeFile(filePath, JSON.stringify(req.body), function(err){
       if (err) {
            return console.log(err);
       }
        res.json({status: 200});
        console.log('json has been saved.');
     });
});

module.exports = router;