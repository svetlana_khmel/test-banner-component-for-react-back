const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const cors = require('cors');
router.use(cors());

const filePath = path.join(__dirname + '../../../../data/data.json');

router.get('/', (req, res, next) => {

  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    if (err) return next(err);
    res.json(JSON.parse(data));
  });
});

module.exports = router;