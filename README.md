# CI backend experemental

Goal of the project:
to create backend for npm-react-banner-component

- Should be set width, height and background image; 

- No need to use database, just write data to JOSN, it is just for test.

** Tools in use:

* Node 
* Express 
* Babel 
* PUG as view engine
* Write file to JSON
* REST


Publish to heroku:

https://git.heroku.com/back-for-test-banner-react-npm.git


*** Helpful articles ***

Create a User Account

To publish, you must be a user on the npm registry. If you aren't a user, create an account by using npm adduser. If you created a user account on the site, use npm login to access your account from your terminal.

Test:

Type npm whoami from a terminal to see if you are already logged in (technically, this also means that your credentials have been stored locally).

Check that your username has been added to the registry at https://npmjs.com/~username.


*To publish package type:

npm publish

* How to create npm package with React:

https://medium.com/@BrodaNoel/how-to-create-a-react-component-and-publish-it-in-npm-668ad7d363ce

Build before publish!!!
